var request = require('supertest');
var app = require('../app.js');
 
describe('GET /', function() {
  it('respond with world', function(done) {
    request(app).get('/').expect('hello world', done);
  });
});